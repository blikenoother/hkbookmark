#fab -f /PATH/fabfile.py -R app app_deploy

from fabric.api import *
from fabric.contrib import files
import os

env.roledefs = {
    'app' : ['root@hkbookmark.pmshould.be:22']
}

env.warn_only = True

app_source = "git@bitbucket.org:blikenoother/hkbookmark.git"

abs_path = os.path.abspath('.')

def __app_deploy_common(env, branch, path):
    if files.exists(path) == False:
        run('mkdir -p %s' % path)
    
    with cd(path):
        if 'fatal' in run('git status'):
            run('git clone %s .' % app_source)
            run('git checkout %s' % branch)
            run('rm -fr assets protected/runtime')
            run('mkdir assets')
            run('mkdir protected/runtime')
            run('chmod 777 -R assets protected/runtime')
            files.put('%s/conf/conf-index.php' % (abs_path), './index.php')
            files.put('%s/conf/conf-htaccess' % (abs_path), './.htaccess')
        else:
            run('git stash')
            run('git checkout %s' % branch)
            run('git pull origin %s' % branch)
        

def app_deploy(branch='master', path="/var/www/hkbookmark.pmshould.be", flush='false'):
    __app_deploy_common(env, branch, path)
    files.sed('%s/protected/config/main.php' % path, 'DATABASE_USERNAME', 'root')
    files.sed('%s/protected/config/main.php' % path, 'DATABASE_PASSWORD', 'db@q1w2e3r4t5')
