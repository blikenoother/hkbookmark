<?php

class BookmarkController extends Controller
{

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$model = new Bookmark;
		$model->unsetAttributes();
		
		// seach query request
		if(isset($_POST['Bookmark']) && isset($_POST['Bookmark']['q'])) {
			$q = $_POST['Bookmark']['q'];
			
			// if query is blank, show users bookmark
			if ($q == '') {
				$model->user_id = Yii::app()->session['user']['id'];
				$gridDataProvider = $model->search();
			} else {
				// get bookmark_id from query text
				$tempBookmarkIds = BookmarkToTag::model()->findAll('tag=:tag', array('tag'=>$q));
				$bookmarkIds = array();
				foreach($tempBookmarkIds as $bm) {
					array_push($bookmarkIds, $bm->bookmark_id);
				}
				
				// user can see his own bookmark or public
				$condition = 'user_id='.Yii::app()->session['user']['id'];
				$condition .= ' OR ';
				$condition .= ' visibility='.Bookmark::VISIBILITY_PUBLIC;
				
				$criteria=new CDbCriteria;
				$criteria->addInCondition('id', $bookmarkIds);
				$criteria->addCondition($condition, 'AND');
				$gridDataProvider = $model->search($criteria);
			}
			
			$this->renderPartial('_bookmark_grid',array('gridDataProvider'=>$gridDataProvider));
			Yii::app()->end();
		}
		
		$model->user_id = Yii::app()->session['user']['id'];
		$gridDataProvider = $model->search();
		
		$this->render('index',array('model'=>$model, 'gridDataProvider'=>$gridDataProvider));
	}
	
	public function actionAdd($id=0) {
		$model = new Bookmark;
		
		if ($id!=0) {
			$model = $model->find('id=:id AND user_id=:userID', array(':id'=>$id, ':userID'=>Yii::app()->session['user']['id']));
			$bookmarkToTag = BookmarkToTag::model()->findAll('bookmark_id=:bookmark_id', array(':bookmark_id'=>$id));
			
			$tags = array();
			foreach($bookmarkToTag as $t)
				array_push($tags, $t->tag);
			
			$model->tags = implode(',', $tags);
		}
		
		if (isset($_GET['operation']) && $_GET['operation']=='delete') {
			BookmarkToTag::model()->deleteAll('bookmark_id=:bookmark_id', array(':bookmark_id'=>$id));
			$model->delete();
			Yii::app()->end();
		}
		
		if (isset($_POST['Bookmark'])) {
			$model->setAttributes($_POST['Bookmark']);
			
			$message = 'Bookmark updated successfully.';
			$new = false;
			if (!isset($model->id)) {
				$model->user_id = Yii::app()->session['user']['id'];
				$model->created_at = date('Y-m-d H:i:s');
				$message = 'New bookmark added successfully.';
				$new = true;
			}
			$model->short_code = crc32($model->url);
			
			if($model->validate() && $model->save()) {
				$tags = str_replace(' ', '', $model->tags);
				$tags = explode(',', $tags);
				$b2t = array();
				foreach($tags as $tag) {
					$row = array('bookmark_id'=>$model->id, 'tag'=>$tag, 'created_at'=>date('Y-m-d H:i:s'));
					array_push($b2t, $row);
				}
				try {
					BookmarkToTag::model()->deleteAll('bookmark_id=:bookmark_id', array(':bookmark_id'=>$model->id));
					BookmarkToTag::model()->saveMultiple($b2t);
				} catch(CDbException $e){}

				Yii::app()->user->setFlash('success', $message);
				if ($new) {
					$model->unsetAttributes();
					$model->tags = '';
				}
			} else {
				Yii::app()->user->setFlash('error', 'Something went wrong,');
			}
		}
		
		$this->render('add',array('model'=>$model));
	}
	
	public function actionClick($id='') {
		if ($id != '') {
			$bookmark = Bookmark::model()->find('short_code=:short_code', array(':short_code'=>$id));
			if ($bookmark) {
				$bookmark->saveCounters(array('clicks'=>1));
				$this->redirect($bookmark->url);
			}
		}
		Yii::app()->end();
	}
}