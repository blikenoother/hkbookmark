<?php

class SiteController extends Controller {
	
	public function beforeAction($action) {
		$action = $this->getAction()->getId();
		if ($action == 'logout')
			return true;
		
		if (isset(Yii::app()->session['user']))
			$this->redirect(array('bookmark/index'));
		return true;
	}
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$model = new User;
		$showName = 0;
		
		if(isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			// new user
			if ( isset($model->name) && $model->name!='') {
				
				$model->status = User::STATUS_ACTIVE;
				$model->created_at = date('Y-m-d H:i:s');
				
				if ($model->validate() && $model->save()) {
					Yii::app()->user->setFlash('success', User::SUCCESS_MSG);
					$model->unsetAttributes();
				} else {
					Yii::app()->user->setFlash('error', User::FAIL_MSG);
				}
			}
			// login
			else {
				if ($model->login()) {
					$this->redirect(array('bookmark/index'));
				} else {
					$showName = 1;
					$model->addError('password','Incorrect username or password.');
				}
			}
		}
		
		
		$this->render('index',array('model'=>$model, 'showName'=>$showName));
	}
	
	public function actionMailexists(){
		$model = new User;
		
		$jsonResp = array('exists'=>0);
		
		$email = isset($_GET['id']) ? $_GET['id'] : '';
		if ($email!='') {
			$n = $model->count('email=:email AND status=:status',
					array(':email'=>$email, ':status'=>User::STATUS_ACTIVE)
			);
			if ($n > 0)
				$jsonResp = array('exists'=>1);
		}

		echo CJSON::encode($jsonResp);
		Yii::app()->end();
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError(){
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->session->destroy();
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}