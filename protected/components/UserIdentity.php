<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user = User::model()->find('BINARY email=:email AND BINARY password=:password AND status=:status',
				array(':email'=>$this->username, ':password'=>$this->password, ':status'=>User::STATUS_ACTIVE));
		
		if ($user) {
			Yii::app()->session['user'] = array('id'=>$user->id, 'name'=>$user->name);
			$this->errorCode=self::ERROR_NONE;
		} else {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		
		return !$this->errorCode;
	}
}