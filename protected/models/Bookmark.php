<?php

/**
 * This is the model class for table "bookmark".
 *
 * The followings are the available columns in table 'bookmark':
 * @property integer $id
 * @property integer $user_id
 * @property string $url
 * @property string $description
 * @property integer $visibility
 * @property string $created_at
 * @property string $short_code
 * @property integer $clicks
 *
 * The followings are the available model relations:
 * @property User $user
 * @property BookmarkToTag[] $bookmarkToTags
 */
class Bookmark extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bookmark the static model class
	 */
	
	public $q;
	public $tags;
	public $decoratedUrl;
	
	const VISIBILITY_PUBLIC = 2;
	const VISIBILITY_PRIVATE = 1;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bookmark';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tags, user_id, url, created_at', 'required'),
			array('id, user_id, visibility', 'numerical', 'integerOnly'=>true),
			array('url', 'url'),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, url, description, visibility, created_at, short_code, clicks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'bookmarkToTags' => array(self::HAS_MANY, 'BookmarkToTag', 'bookmark_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'url' => 'Url',
			'description' => 'Description',
			'visibility' => 'Public',
			'created_at' => 'Created At',
			'q' => 'Discover new URL',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($criteria=null, $pagination=array('pageSize' => 5, 'validateCurrentPage'=>false)){
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		if ($criteria == null) {
			$criteria=new CDbCriteria;
			$criteria->compare('user_id',$this->user_id);
			$criteria->compare('url',$this->url,true);
			$criteria->compare('created_at',$this->created_at,true);
			$criteria->compare('clicks',$this->clicks);
			$criteria->with = array('bookmarkToTags');
			$criteria->together = true;
		}
		
		$dataProvider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>$pagination
		));
		
		$data = array();
		foreach($dataProvider->data as $d) {
			$d->decoratedUrl = '<a href="click/'.$d->short_code.'" target="_blank" >'.$d->url.'</a>';
			$tags = array();
			
			foreach ($d->bookmarkToTags as $bookMark)
				array_push($tags, $bookMark->tag);
			
			$d->tags = implode(',', $tags);
			array_push($data, $d);
		}
		
		$dataProvider->setData($data);
		
		return $dataProvider;
	}
	
	public static function getVisibilityIcon($visibility) {
		if ($visibility == Bookmark::VISIBILITY_PUBLIC) {
			return '<i class="icon-eye-open" title="Public">';
		} else {
			return '<i class="icon-eye-close" title="Private">';
		}
	}
	
	public static function getClickUrl($shortCode, $url) {
		return '<a href="'.$shortCode.'" target="_blank" >'.$url.'</a>';
	}
}