<div class="form">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'user-form',
	    'type'=>'horizontal',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>
	
	<div id="ajax_loading"><img alt="Loading" src="<?php echo Yii::app()->theme->baseUrl;?>/images/ajax-loader.gif" width="20" /></div>
	<?php echo $form->textFieldRow($model,'email', array('id'=>'email')); ?>
	<div class="signup_field">
		<?php echo $form->textFieldRow($model,'name', array('id'=>'name')); ?>
	</div>
	<?php echo $form->passwordFieldRow($model,'password', array('id'=>'password')); ?>
	
	<div style="margin-left: 200px;">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Signup',
			'htmlOptions'=>array('id'=>'submit_btn'),
        )); ?>
    </div>
    
	<?php $this->endWidget(); ?>
</div>