<?php
$this->pageTitle=Yii::app()->name;
?>

<?php $this->beginWidget('bootstrap.widgets.TbHeroUnit',array(
    'heading'=>'Welcome to '.CHtml::encode(Yii::app()->name),
)); ?>

<?php $this->endWidget(); ?>

<?php $this->renderPartial('/common/_message'); ?>

<?php $this->renderPartial('_login', array('model'=>$model)); ?>

<script type="text/javascript">
$(document).ready(function(){

	$("#ajax_loading").bind("ajaxStart", function(){
 		$(this).show();
 	}).bind("ajaxStop", function(){
 		$(this).hide();
	});
	
	var showName = '<?php echo $showName?>';
	if (showName == 1) {
		$('.signup_field').hide();
    	$('#name').val('');
    	$('#submit_btn').html('Login');
	}
	
	$('#email').focusout(function(){
		var ajaxUrl = "<?php echo $this->createAbsoluteUrl('site/mailexists'); ?>";
		ajaxUrl += '?id='+$(this).val();
		$.ajax({
			url: ajaxUrl,
	        type: 'GET',
	        dataType: 'json',
	        success: function(response, status) {
	        	if (response.exists == 1) {
		        	$('.signup_field').hide();
		        	$('#name').val('');
		        	$('#password').focus();
		        	$('#submit_btn').html('Login');
	        	} else {
	        		$('.signup_field').show();
	        		$('#submit_btn').html('Signup');
	        	}
	        }
	    });
	});
});
</script>