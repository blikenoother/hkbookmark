<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$gridDataProvider,
    'template'=>"{items} {summary} {pager}",
	'pagerCssClass'=>'pagination profilelv-pager',
	'summaryCssClass'=>'profilelv-summary',
    'columns'=>array(
        array('name'=>'url',
        	'header'=>'URL',
        	'type'=>'raw',
        	'value'=>'$data->decoratedUrl',
        ),
        array('name'=>'tags', 'header'=>'Tags'),
    	array('name'=>'description',
    		'header'=>'Description',
			'type'=>'raw',
			'value'=>'$data->description',
			'sortable'=>false
    	),
    	array('name'=>'visibility',
    		'header'=>'Visibility',
    		'type'=>'raw',
    		'value'=>'Bookmark::getVisibilityIcon($data->visibility);',
    	),
    		array('name'=>'clicks', 'header'=>'Clicks'),
    	array('name'=>'created_at', 'header'=>'Date Time', 'value'=>'date("d-m-Y H:i",strtotime($data->created_at))'),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
        	'template'=>'{update}&nbsp;&nbsp;{delete}',
        	'buttons'=>array(
        		'update' => array(
        			'url'=>'Yii::app()->controller->createUrl("bookmark/add", array("id"=>$data->id))',
        			'visible'=>'$data->user_id==Yii::app()->session["user"]["id"]',
        		),
        		'delete' => array(
        			'url'=>'Yii::app()->controller->createUrl("bookmark/add", array("operation"=>"delete","id"=>$data->id))',
        			'visible'=>'$data->user_id==Yii::app()->session["user"]["id"]',
        		),
        	),
            'htmlOptions'=>array('style'=>'width: 50px'),
        ),
    ),
)); ?>