<?php $this->pageTitle=Yii::app()->name;

// button
$this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Add New',
    'type'=>'primary',
    'size'=>'large',
	'htmlOptions'=>array('style'=>'margin-bottom: 20px;'),
	'url'=>Yii::app()->controller->createUrl("bookmark/add"),
));?>

<!-- search -->
<div style="float: right; margin-top: 20px;">
	<?php $this->renderPartial('_search', array('model'=>$model)); ?>
</div>
<div class="clear"></div>

<!-- grid data -->
<div id="bookmark_grid">
	<?php $this->renderPartial('_bookmark_grid', array('gridDataProvider'=>$gridDataProvider)); ?>
</div>