<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'bookmark-form',
	'type'=>'search',
));
echo $form->textFieldRow($model, 'q', array('class'=>'input-medium', 'prepend'=>'<i class="icon-search"></i>'));
echo '&nbsp;&nbsp;';
$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'ajaxSubmit',
		'label'=>'Go',
		'loadingText'=>'Searching..',
		'completeText'=>'Go',
		'ajaxOptions'=>array('success' => 'function(data){ $("#bookmark_grid").html(data); }'),
	));
$this->endWidget();

