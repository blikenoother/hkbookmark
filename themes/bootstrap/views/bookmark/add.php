<?php
$this->pageTitle=Yii::app()->name;

$this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'View',
    'type'=>'primary',
    'size'=>'large',
	'htmlOptions'=>array('style'=>'margin-bottom: 20px;'),
	'url'=>Yii::app()->controller->createUrl("index"),
));

$this->renderPartial('/common/_message'); ?>

<div class="form">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'bookmark-form',
	    'type'=>'horizontal',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>
	
	<?php echo $form->errorSummary($model); ?>
	
	<?php echo isset($model->id) ? $form->hiddenField($model, 'id') : ''?>
	<?php echo $form->textFieldRow($model,'url', array('class'=>'span4')); ?>
	<?php echo $form->textFieldRow($model,'tags', array('class'=>'span4', 'hint'=>'You can add multiple tags with "," seprated. Eg: tag1,tag2')); ?>
	<?php echo $form->textAreaRow($model,'description', array('class'=>'span4')); ?>
	<?php echo $form->checkBoxRow($model, 'visibility', array('value'=>Bookmark::VISIBILITY_PUBLIC, 'uncheckValue'=>Bookmark::VISIBILITY_PRIVATE)); ?>
	
	<?php $btnLbl = isset($model->id) ? 'Update' : 'Add'?>
	
	<div style="margin-left: 200px;">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Add',
			'htmlOptions'=>array('id'=>'submit_btn'),
        )); ?>
    </div>
    
	<?php $this->endWidget(); ?>
</div>